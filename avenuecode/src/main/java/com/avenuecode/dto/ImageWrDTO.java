package com.avenuecode.dto;

public class ImageWrDTO extends AbstractDTO{

	private String type;
	private Byte image;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Byte getImage() {
		return image;
	}
	public void setImage(Byte image) {
		this.image = image;
	}
	
	
}
