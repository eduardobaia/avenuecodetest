package com.avenuecode.dto;


public class ImageDTO extends AbstractDTO{

	private String type;
	private Byte image;
	private ProductDTO product;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Byte getImage() {
		return image;
	}
	public void setImage(Byte image) {
		this.image = image;
	}
	public ProductDTO getProduct() {
		return product;
	}
	public void setProduct(ProductDTO product) {
		this.product = product;
	}

	
	
}
