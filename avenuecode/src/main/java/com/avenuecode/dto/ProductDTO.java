package com.avenuecode.dto;

import java.util.Collection;

import com.avenuecode.model.Image;

public class ProductDTO extends AbstractDTO{

	private String name;
	private String description;
	private Collection<ImageWrDTO> images;
	private ProductDTO parentProduct;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Collection<ImageWrDTO> getImages() {
		return images;
	}
	public void setImages(Collection<ImageWrDTO> images) {
		this.images = images;
	}
	public ProductDTO getParentProduct() {
		return parentProduct;
	}
	public void setParentProduct(ProductDTO parentProduct) {
		this.parentProduct = parentProduct;
	}
	
	
	
	
	
}
