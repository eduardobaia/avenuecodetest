package com.avenuecode.exception;

import java.util.ArrayList;
import java.util.List;

public class ImageExceptionHandler {

	public static List<String> getExcetionError(final Exception e){
		final List<String> exceptions= new ArrayList<>();
		if (e instanceof MultipleImageException) {
			final MultipleImageException me = (MultipleImageException) e;
			for (final ImageException exception : me.getExceptions()) {
				exceptions.add(exception.getMessage());
			}
		}else if (e instanceof ImageException){
			final ImageException exception = (ImageException) e;
			exceptions.add(exception.getMessage());
		}else{
			exceptions.add("generic error");
		}
		return exceptions;
	}
}
