package com.avenuecode.exception;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class MultipleImageException extends RuntimeException{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 7139972070262446093L;

	/**
	 * Lista de excecoes
	 */
	private final Set<ImageException> exceptions = new HashSet<ImageException>();

	/**
	 * Constroi uma instancia desta classe.
	 *
	 * @param message mensagem de erro
	 */
	public MultipleImageException(final ImageException message) {
		exceptions.add(message);
	}

	/**
	 * Constroi uma instancia desta classe.
	 *
	 * @param messageList mensagem de erro
	 */
	public MultipleImageException(final Collection<? extends ImageException> messageList) {
		exceptions.addAll(messageList);
	}

	public MultipleImageException() {
		//Construtor padrao
	}


	/**
	 * Setter of parametters .
	 *
	 * @param message a mensagem de detalhe
	 */
	public void addException(final ImageException message) {
		exceptions.add(message);
	}
//TODO traduzir
	/**
	 * Setter dos parametros para formatacao da mensagem.
	 *
	 * @param messageList a mensagem de detalhe
	 */
	public void addExceptionList(final Collection<? extends ImageException> messageList) {
		exceptions.addAll(messageList);
	}

	public Set<ImageException> getExceptions() {
		return exceptions;
	}

	public boolean contains(final ImageException ex) {
		return (null != exceptions && !exceptions.isEmpty()) && exceptions.contains(ex);
	}
}
