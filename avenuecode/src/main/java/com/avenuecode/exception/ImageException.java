package com.avenuecode.exception;


public class ImageException extends Exception{

	private static final long serialVersionUID = 1L;

	public ImageException() {
	}

	public ImageException(final String message) {
		super(message);
	}

	public ImageException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public ImageException(final Throwable cause) {
		super(cause);
	}
}
