package com.avenuecode.controller;

import java.text.ParseException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.converter.ImageConverter;
import com.avenuecode.dto.ImageDTO;
import com.avenuecode.exception.Error;
import com.avenuecode.exception.ImageExceptionHandler;
import com.avenuecode.service.ImageService;


@RequestMapping("/image")
@CrossOrigin
@RestController
public class ImageController {


	@Autowired
	private ImageService imageService;
	
	
	@RequestMapping(method = RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findAll(@RequestParam final Map<String,String> params) throws ParseException{
		try{
			if(params.isEmpty()){
				return new ResponseEntity<>(imageService.findAll(), HttpStatus.OK);
			}else{
				return new ResponseEntity<>(imageService.findByFilter(ImageConverter.toDTO(params)), HttpStatus.OK);
		}
		}catch (final RuntimeException e){
			return new ResponseEntity<Error>(new Error(1, ImageExceptionHandler.getExcetionError(e)), HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findById(@PathVariable final Long id){
		try {
			final ImageDTO dto = imageService.findById(id);
			return new ResponseEntity<ImageDTO>(dto, HttpStatus.OK);
		} catch (final RuntimeException e) {
			return new ResponseEntity<Error>(new Error(1, ImageExceptionHandler.getExcetionError(e)), HttpStatus.EXPECTATION_FAILED);
		}
	}

	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.POST , consumes = MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> persist(@RequestBody final ImageDTO dto){
		try {
			final ImageDTO inserted= imageService.persist(dto);
			return new ResponseEntity<ImageDTO>(inserted, HttpStatus.OK);
		} catch (final RuntimeException e) {
			return new ResponseEntity<Error>(new Error(1, ImageExceptionHandler.getExcetionError(e)), HttpStatus.EXPECTATION_FAILED);
		}
	}

	
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> update(@RequestBody final ImageDTO dto){
		try {
			final ImageDTO updated= imageService.update(dto);
			return new ResponseEntity<ImageDTO>(updated, HttpStatus.OK);
		} catch (final RuntimeException e) {
			return new ResponseEntity<Error>(new Error(1, ImageExceptionHandler.getExcetionError(e)), HttpStatus.EXPECTATION_FAILED);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE , produces=MediaType.APPLICATION_JSON_VALUE)
	public void delete(@PathVariable final Long id){
		imageService.delete(id);
	}
	
	
}
