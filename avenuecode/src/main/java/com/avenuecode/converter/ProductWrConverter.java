package com.avenuecode.converter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.avenuecode.dto.ProductWrDTO;
import com.avenuecode.model.Product;

public class ProductWrConverter {

	public static ProductWrDTO toDTO(final Product product){
		final ProductWrDTO productWrDTO = new ProductWrDTO();
		productWrDTO.setId(product.getId());
		productWrDTO.setName(product.getName());
		productWrDTO.setDescription(product.getDescription());

	
		return productWrDTO;
	}
	
	public static List<ProductWrDTO> toListDTO(final List<Product> list){
		final List<ProductWrDTO> results = new ArrayList<>();
		for(final Product product: list){
			results.add(toDTO(product));
		}
		return results;
	}
	
	
	
	public static ProductWrDTO toDTO(final Map<String, String> params) throws ParseException{
		final ProductWrDTO toReturn = new ProductWrDTO();
//		final ProductDTO parentProductDTO = new ProductDTO();
//		toReturn.setParentProduct(parentProductDTO);
//		
		for(final Map.Entry<String, String> entry : params.entrySet()){
		
			if(entry.getKey().equalsIgnoreCase("name")){
				toReturn.setName(entry.getValue());
			}
	
		
		
		}
		return toReturn;
	}
}
