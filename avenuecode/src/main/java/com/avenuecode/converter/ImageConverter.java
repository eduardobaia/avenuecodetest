package com.avenuecode.converter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.avenuecode.dto.ImageDTO;
import com.avenuecode.dto.ProductDTO;
import com.avenuecode.model.Image;

public class ImageConverter {

	
	public static Image toModel(final ImageDTO imageDTO){
		final Image image = new Image();
		image.setId(imageDTO.getId());
		image.setType(imageDTO.getType());
		if(imageDTO.getProduct() != null){
			image.setProduct(ProductConverter.toModel(imageDTO.getProduct()));
		}
		image.setImage(imageDTO.getImage());
		
		return image;
	}
	
	
	public static ImageDTO toDTO(final Image image){
		final ImageDTO imageDTO= new ImageDTO();
		imageDTO.setId(image.getId());
		imageDTO.setType(image.getType());
		if(image.getProduct() != null){
			imageDTO.setProduct(ProductConverter.toDTO(image.getProduct()));
		}
		
		imageDTO.setImage(image.getImage());
		return imageDTO;
	}
	
	
	public static List<ImageDTO> toListDTO(final List<Image> list){
		final List<ImageDTO> results = new ArrayList<>();
		for(final Image image : list){
			results.add(toDTO(image));
		}
		
		return results;
	}
	
	public static List<Image> toListModel(final List<ImageDTO> list){
		final List<Image> results = new ArrayList<>();
		for(final ImageDTO image : list){
			results.add(toModel(image));
		}
		return results;
	}
	
	public static ImageDTO toDTO(final Map<String, String> params) throws ParseException{
		final ImageDTO toReturn = new ImageDTO();
		final ProductDTO product = new ProductDTO();
		toReturn.setProduct(product);
		
		for(final Map.Entry<String, String> entry : params.entrySet()){
			
			if(entry.getKey().equalsIgnoreCase("type")){
				toReturn.setType(entry.getValue());
			}
			if(entry.getKey().equalsIgnoreCase("product.id")){
				toReturn.getProduct().setId(Long.parseLong(entry.getValue()));
			}
			
		}
	
		return toReturn;
	}
}
