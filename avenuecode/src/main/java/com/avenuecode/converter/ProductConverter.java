package com.avenuecode.converter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.avenuecode.dto.ProductDTO;
import com.avenuecode.model.Image;
import com.avenuecode.model.Product;

public class ProductConverter {

	public static Product toModel(final ProductDTO productDTO){
		final Product product = new Product();
		product.setId(productDTO.getId());
		product.setName(productDTO.getName());
		product.setDescription(productDTO.getDescription());

		if(productDTO.getParentProduct() != null){
			product.setParentProduct(ProductConverter.toModel(productDTO.getParentProduct()));
		}
		return product;
	}
	
	
	public static ProductDTO toDTO(final Product product){
		final ProductDTO productDTO = new ProductDTO();
		productDTO.setId(product.getId());
		productDTO.setName(product.getName());
		productDTO.setDescription(product.getDescription());
//		if(product.getImages() != null){
//			productDTO.setImages(ImageConverter.toListDTO((List<Image>) product.getImages()));
//		}
		
		if(product.getImages() != null){
			productDTO.setImages(ImageWrConverter.toListDTO((List<Image>) product.getImages()));
		}
		if(product.getParentProduct() !=null){
			productDTO.setParentProduct(ProductConverter.toDTO(product.getParentProduct()));
		}
	
		return productDTO;
	}
	
	public static List<ProductDTO> toListDTO(final List<Product> list){
		final List<ProductDTO> results = new ArrayList<>();
		for(final Product product: list){
			results.add(toDTO(product));
		}
		return results;
	}
	
	
	public static Set<Product> toListModel(final List<ProductDTO> list){
		final Set<Product> results = new HashSet<>();
		for (final ProductDTO product : list) {
			results.add(toModel(product));
		}
		return results;		
	}
	
	
	public static ProductDTO toDTO(final Map<String, String> params) throws ParseException{
		final ProductDTO toReturn = new ProductDTO();
		final ProductDTO parentProductDTO = new ProductDTO();
		toReturn.setParentProduct(parentProductDTO);
		
		for(final Map.Entry<String, String> entry : params.entrySet()){
		
			if(entry.getKey().equalsIgnoreCase("name")){
				toReturn.setName(entry.getValue());
			}
			if(entry.getKey().equalsIgnoreCase("description")){
				toReturn.setDescription(entry.getValue());
			}
			if(entry.getKey().equalsIgnoreCase("parentProduct.id")){
				toReturn.getParentProduct().setId(Long.parseLong(entry.getValue()));
			}
		
		}
		return toReturn;
	}
}
