package com.avenuecode.converter;

import java.util.ArrayList;
import java.util.List;

import com.avenuecode.dto.ImageWrDTO;
import com.avenuecode.model.Image;

public class ImageWrConverter {

	//The purpose of this class is to consult the relationship it has with the product
	
	
	public static ImageWrDTO toDTO(final Image image){
		final ImageWrDTO imageDTO= new ImageWrDTO();
		imageDTO.setId(image.getId());
		imageDTO.setType(image.getType());
		
		imageDTO.setImage(image.getImage());
		return imageDTO;
	}
	
	
	public static List<ImageWrDTO> toListDTO(final List<Image> list){
		final List<ImageWrDTO> results = new ArrayList<>();
		for(final Image image : list){
			results.add(toDTO(image));
		}
		
		return results;
	}
	
	
	
}
