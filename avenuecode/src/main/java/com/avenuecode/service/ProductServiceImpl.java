package com.avenuecode.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.avenuecode.converter.ProductConverter;
import com.avenuecode.converter.ProductWrConverter;
import com.avenuecode.dto.ProductDTO;
import com.avenuecode.dto.ProductWrDTO;
import com.avenuecode.repository.ProductRepository;

@Transactional(readOnly = true)
@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public List<ProductDTO> findByFilter(ProductDTO productDTO) {
		return ProductConverter.toListDTO(productRepository.findByFilter(ProductConverter.toModel(productDTO)));
	}

	@Override
	public List<ProductDTO> findAll() {
		return ProductConverter.toListDTO(productRepository.findAll());
	}
	
	@Override
	public List<ProductWrDTO> findWrAll() {
		return ProductWrConverter.toListDTO(productRepository.findAll());
	}

	@Override
	public ProductDTO findById(final Long id) {
		return ProductConverter.toDTO(productRepository.findOne(id));
	}

	@Override
	public ProductWrDTO findWrById(final Long id) {
		return ProductWrConverter.toDTO(productRepository.findOne(id));
	}
	
	@Transactional(readOnly = false)
	@Override
	public ProductDTO updateProduct(final ProductDTO productDTO) {
		return ProductConverter.toDTO(productRepository.save(ProductConverter.toModel(productDTO)));
	}
	
	
	@Transactional(readOnly = false)
	@Override
	public ProductDTO persistProduct(final ProductDTO productDTO) {
		return ProductConverter.toDTO(productRepository.save(ProductConverter.toModel(productDTO)));
		}

	@Transactional(readOnly = false)
	@Override
	public void deleteProduct(final Long idProduct) {
		productRepository.delete(idProduct);
	}


}
