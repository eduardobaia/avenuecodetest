package com.avenuecode.service;

import java.util.List;

import com.avenuecode.dto.ImageDTO;

public interface ImageService {

	List<ImageDTO> findByFilter(ImageDTO imageDTO);

	List<ImageDTO> findAll();

	ImageDTO findById(Long id);

	ImageDTO update(ImageDTO imageDTO);

	void delete(Long id);

	ImageDTO persist(ImageDTO imageDTO);
}

