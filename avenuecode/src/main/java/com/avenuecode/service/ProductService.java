package com.avenuecode.service;

import java.util.List;

import com.avenuecode.dto.ProductDTO;
import com.avenuecode.dto.ProductWrDTO;


public interface ProductService {

	List<ProductDTO> findByFilter(ProductDTO productDTO);

	List<ProductDTO> findAll();
	
	List<ProductWrDTO> findWrAll();

	ProductDTO findById(Long id);

	ProductWrDTO findWrById(Long id);
	
	ProductDTO updateProduct(ProductDTO productDTO);
	
	ProductDTO persistProduct(ProductDTO productDTO);
	
	void deleteProduct(Long idProduct);
}
