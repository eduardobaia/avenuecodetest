package com.avenuecode.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.avenuecode.converter.ImageConverter;
import com.avenuecode.dto.ImageDTO;
import com.avenuecode.repository.ImageRepository;

@Transactional(readOnly = true)
@Service
public class ImageServiceImpl implements ImageService{

	@Autowired
	private ImageRepository imageRepository;
	
	
	@Override
	public List<ImageDTO> findByFilter(ImageDTO imageDTO) {
		return ImageConverter.toListDTO(imageRepository.findByFilter(ImageConverter.toModel(imageDTO)));
	}

	@Override
	public List<ImageDTO> findAll() {
		return ImageConverter.toListDTO(imageRepository.findAll());
	}

	@Override
	public ImageDTO findById(final Long id) {
		return ImageConverter.toDTO(imageRepository.findOne(id));
	}

	@Transactional(readOnly = false)
	@Override
	public ImageDTO update(final ImageDTO imageDTO) {
		return ImageConverter.toDTO(imageRepository.save(ImageConverter.toModel(imageDTO)));
	}

	@Transactional(readOnly = false)
	@Override
	public void delete(final Long id) {
		imageRepository.delete(id);
	}

	@Transactional(readOnly = false)
	@Override
	public ImageDTO persist(final ImageDTO imageDTO) {
		return ImageConverter.toDTO(imageRepository.save(ImageConverter.toModel(imageDTO)));
	}

}
