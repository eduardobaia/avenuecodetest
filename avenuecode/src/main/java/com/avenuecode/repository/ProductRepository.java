package com.avenuecode.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avenuecode.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>, ProductRepositoryCustom{

}
