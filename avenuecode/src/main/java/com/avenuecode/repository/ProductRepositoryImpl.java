package com.avenuecode.repository;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.avenuecode.model.Product;

public class ProductRepositoryImpl extends AbstractRepository implements ProductRepositoryCustom {
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> findByFilter(Product product) {
		final Criteria criteria = createCriteria(Product.class);

		if(StringUtils.isNotBlank(product.getName())){
			criteria.add(Restrictions.ilike("name", product.getName(),  MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(product.getDescription())){
			criteria.add(Restrictions.ilike("description", product.getDescription(),  MatchMode.ANYWHERE));
		}
		
		if (product.getParentProduct() != null && product.getParentProduct().getId() != null) {
				criteria.add(Restrictions.eq("parentProduct.id",product.getParentProduct().getId()));
		}

		return criteria.list();
	}

}
