package com.avenuecode.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;

public class AbstractRepository {

	@PersistenceContext
	protected EntityManager em;

	protected Criteria createCriteria(final Class<?> persistenceClass) {
		return em.unwrap(Session.class).createCriteria(persistenceClass);
	}
}
