package com.avenuecode.repository;

import java.util.List;

import com.avenuecode.model.Product;

public interface ProductRepositoryCustom {

	List<Product> findByFilter(Product product);
}
