package com.avenuecode.repository;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.avenuecode.model.Image;

public class ImageRepositoryImpl extends AbstractRepository implements ImageRepositoryCustom{
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Image> findByFilter(Image image) {
		final Criteria criteria = createCriteria(Image.class);
		
		if(StringUtils.isNotBlank(image.getType())){
			criteria.add(Restrictions.ilike("type", image.getType(), MatchMode.ANYWHERE));
		}
		if(image.getProduct() != null && image.getProduct().getId() != null){
			criteria.add(Restrictions.eq("product.id", image.getProduct().getId()));
		}
		return criteria.list();
	}

}
