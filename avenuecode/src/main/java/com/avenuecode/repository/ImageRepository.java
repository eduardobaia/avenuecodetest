package com.avenuecode.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avenuecode.model.Image;

public interface ImageRepository extends JpaRepository<Image, Long>, ImageRepositoryCustom{

}
