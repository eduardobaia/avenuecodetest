package com.avenuecode.repository;

import java.util.List;

import com.avenuecode.model.Image;


public interface ImageRepositoryCustom {
	
	List<Image> findByFilter(Image image);

}
