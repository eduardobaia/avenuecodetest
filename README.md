
# README #

This README would normally document whatever steps are necessary to get your application up and running.

To run:

mvn clean install

mvn spring-boot:run

### 1)  Create, update and delete products ###

Endpoints Product

* http://localhost:8080/product 
(POST) 

Parent Product

    {   
        "name":"Television",
        "description":"Samsung"
    }
    Product chield

    {  
    "name":"Television  40x",
    "description":"Samsung",
    "parentProduct": {
	                "id":1	
                    }
   
}



*http://localhost:8080/product (PUT)

    {  
	"id":3,
    "name":"Television  55x",
    "description":"Philips",
    "parentProduct": {
    	"id":1    	
        }
   
    }


*http://localhost:8080/product/{id} (DELETE)

*http://localhost:8080/product{id}  (GET BY ID)

### 4) Get all products including specified relationships (child product and/or images) ###
*http://localhost:8080/product  (GET)
*http://localhost:8080/product{id}  (GET BY ID)


### 3) Get all products excluding relationships (child products, images) ###

*Wr = Without relationships
*http://localhost:8080/product/wr/ (GET)

### 5) Same as 3 using specific product identity

*http://localhost:8080/product/wr/{id} (GET)


### 7) Get set of child products for specific product ###

*http://localhost:8080/product/?parentProduct.id={id} (GET)



### 2) Create, update and delete images ###

Endpoints Image

*http://localhost:8080/image (POST)

    {      
     "type":"PNG"  
    }
	  
	  
*http://localhost:8080/image (PUT)

	{  
        "id":1,
         "type":"JPEG"	,
    	 "product": {"id":1}
      }	 
	  
	  
*http://localhost:8080/image{id} (GET)

*http://localhost:8080/image/{id} (DELETE)


### 8) Get  of images for specific product  ###


*http://localhost:8080/image/?product.id={id} (GET)









